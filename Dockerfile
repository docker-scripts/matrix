include(bookworm)

RUN apt update && apt upgrade --yes

### install dependencies
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        build-essential \
        python3-dev \
        libffi-dev \
        python3-pip \
        python3-setuptools \
        postgresql \
        libpq5 \
        sqlite3 \
        libssl-dev \
        virtualenv \
        libjpeg-dev \
        libxslt1-dev \
        nginx
RUN cp -a /var/lib/postgresql /var/lib/postgresql.initial

### install matrix-synapse
RUN apt install --yes \
    lsb-release wget apt-transport-https
RUN wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg \
        https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg &&\
    echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" \
        > /etc/apt/sources.list.d/matrix-org.list &&\
    apt update &&\
    DEBIAN_FRONTEND=noninteractive \
    apt install --yes matrix-synapse-py3

### install synapse-admin
RUN wget $(wget -qO- https://api.github.com/repos/Awesome-Technologies/synapse-admin/releases/latest | grep browser_download_url | cut -d : -f 2,3 | tr -d '"') &&\
    tar xfz synapse-admin-*.tar.gz &&\
    rm -f synapse-admin-*.tar.gz &&\
    mv synapse-admin-* /var/www/html/synapse-admin