#!/bin/bash -x

source /host/settings.sh
CONFIG=/etc/matrix-synapse/conf.d

main() {
    setup_matrix
    setup_db
    setup_nginx
}

setup_matrix() {
    # don't overwrite
    [[ -f $CONFIG/matrix.yaml ]] && return

    cat > $CONFIG/matrix.yaml <<EOF
# basic setup
server_name: $DOMAIN
report_stats: false
serve_server_wellknown: true
EOF

    setup_email
    fix_bind_addresses
    set_registration_shared_secret
    enable_url_previews

    systemctl restart matrix-synapse
}

setup_email() {
    # don't overwrite
    [[ -f $CONFIG/email.yaml ]] && return

    cat > $CONFIG/email.yaml <<EOF
email:
  smtp_host: $SMTP_HOST
  smtp_port: $SMTP_PORT
  notif_from: $NOTIF_FROM
  #smtp_user:
  #smtp_pass:
EOF
}

fix_bind_addresses() {    
    # The default value for bind_addresses is ['::1', '127.0.0.1']
    # which prevents the service from starting, because IPv6
    # is not enabled on the container.

    cat > $CONFIG/listeners.yaml <<EOF
listeners:
  - port: 8008
    tls: false
    type: http
    x_forwarded: true
    bind_addresses: ['127.0.0.1']

    resources:
      - names: [client, federation]
        compress: false
EOF
}

gen_secret() {
    local length=$1
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $length | head -n 1
}

set_registration_shared_secret() {
    # don't overwrite
    [[ -f $CONFIG/registration_shared_secret.yaml ]] && return
    
    local secret=$(gen_secret 32)
    cat > $CONFIG/registration_shared_secret.yaml <<EOF
registration_shared_secret: $secret
EOF
}

enable_url_previews() {
    # don't overwrite
    [[ -f $CONFIG/url_preview.yaml ]] && return
    
    cat > $CONFIG/url_preview.yaml <<EOF
url_preview_enabled: true
url_preview_ip_range_blacklist:
  - '127.0.0.0/8'
  - '10.0.0.0/8'
  - '172.16.0.0/12'
  - '192.168.0.0/16'
  - '100.64.0.0/10'
  - '192.0.0.0/24'
  - '169.254.0.0/16'
  - '192.88.99.0/24'
  - '198.18.0.0/15'
  - '192.0.2.0/24'
  - '198.51.100.0/24'
  - '203.0.113.0/24'
  - '224.0.0.0/4'
  - '::1/128'
  - 'fe80::/10'
  - 'fc00::/7'
  - '2001:db8::/32'
  - 'ff00::/8'
  - 'fec0::/10'
EOF
}

setup_db() {
    # check whether the db already exists
    if [[ -n $(ls /var/lib/postgresql) ]]; then
        chown postgres -R /var/lib/postgresql/
        return
    fi

    # make sure that the service is running
    cp -a /var/lib/postgresql.initial/* /var/lib/postgresql/
    chown postgres -R /var/lib/postgresql/
    systemctl start postgresql

    # create a user and a database
    su - postgres -c 'createuser synapse_user'
    local passwd=$(gen_secret 10)
    su - postgres -c "echo \"ALTER USER synapse_user WITH PASSWORD '$passwd'\" | psql"
    su - postgres -c 'createdb --encoding=UTF8 --locale=C --template=template0 --owner=synapse_user synapse'

    # setup synapse config
    cat > $CONFIG/database.yaml <<EOF
database:
  name: psycopg2
  args:
    user: synapse_user
    password: $passwd
    database: synapse
    host: 127.0.0.1
    cp_min: 5
    cp_max: 10
EOF
}

setup_nginx() {
    cat > /etc/nginx/sites-available/matrix <<'EOF'
server {
    listen 80;
    server_name matrix.example.org;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    server_name matrix.example.org;

    #location ~ ^(/health|/.well-known/matrix|/_matrix|/_synapse/client|/_synapse/admin) {
    location ~ ^/ {
        # note: do not add a path (even a single /) after the port in `proxy_pass`,
        # otherwise nginx will canonicalise the URI and cause signature verification
        # errors.
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;
    }

    location /admin {
        alias /var/www/html/synapse-admin;
        index index.html;
    }

    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;
}
EOF
    
    sed -i /etc/nginx/sites-available/matrix \
        -e "s/server_name matrix.example.org;/server_name $DOMAIN;/"
    ln -s /etc/nginx/sites-available/matrix /etc/nginx/sites-enabled/

    systemctl restart nginx
}

# call main
main "$@"
