rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p postgresql config
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/postgresql,dst=/var/lib/postgresql \
        --mount type=bind,src=$(pwd)/config,dst=/etc/matrix-synapse/conf.d \
        "$@"
#        --publish 1194:1194/tcp --publish 1194:1194/udp \
#        --publish 49160-49200:49160-49200/udp \
}
