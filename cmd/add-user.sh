cmd_add-user_help() {
    cat <<_EOF
    add-user <username> <password> [--admin]
        Create a new user with the given password.
        By default the user is created as non-admin,
        use the option --admin to create an admin user.

_EOF
}

cmd_add-user() {
    [[ $# -lt 2 ]] && fail "Usage:\n$(cmd_add-user_help)"
    
    local username="$1"
    local password="$2"
    local admin=${3:- --no-admin}
    ds shell register_new_matrix_user \
       --config /etc/matrix-synapse/conf.d/registration_shared_secret.yaml \
       --user "$username" \
       --password "$password" \
       $admin \
       http://localhost:8008
}
