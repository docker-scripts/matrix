# Matrix in a container

https://matrix-org.github.io/synapse/

## Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull matrix`

  - Create a directory for the container: `ds init matrix @matrix.example.org`

  - Fix the settings: `cd /var/ds/matrix.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

## Testing

```bash
curl https://matrix.example.org/health
curl https://matrix.example.org/_synapse/admin/v1/server_version
```

https://federationtester.matrix.org/#matrix.example.org

## Usage

- Create a new user: `ds add-user <username> <password> [--admin]`
- Use synapse-admin interface on: https://matrix.example.org/admin 


## Other commands

```
ds stop
ds start
ds shell
```
